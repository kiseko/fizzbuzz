package jp.alhinc.koseki_motoyuki.FizzBuzz;

import static org.junit.Assert.*;

import org.junit.Test;

public class FizzBuzzTest {

	@Test
	public void testCheckFizzBuzz_01() {
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	@Test
	public void testCheckFizzBuzz_02() {
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	@Test
	public void testCheckFizzBuzz_03() {
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}

	@Test
	public void testCheckFizzBuzz_04() {
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}

	@Test
	public void testCheckFizzBuzz_05() {
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));

	}
}
